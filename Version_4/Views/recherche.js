console.log("Ce programme JS vient d'être chargé");
$(document).ready(function()
{
	console.log("Le document est pret");
    
    $('#bouton_recherche').mousedown(function(){
       $.get('?controller=recherche&action=Recherche',
            {
                input_nom: $('#input_nom').val(),
                input_prenom: $("#input_prenom").val(),
				type_contrat: $("#type_contrat").val(),
				annee_fin: $("#annee_fin").val(),
				annee_debut: $("#annee_debut").val(),
				matieres: $("#matieres").val()
            },
             function(reponse){
                $("#resultats_recherche").html("");
                console.log(reponse);
                var arrayRep = jQuery.parseJSON(reponse)["test"];
                console.log(arrayRep);
                for (var i = 0; i<arrayRep.length;i+=1){
                    
                    var genre = arrayRep[i]["genre"];
                    console.log(genre);
                    if (genre=='homme') {
                        genre="Mr."
                    }
                    else if (genre=='femme'){
                        genre='Mme.';
                    }
                    var nom = arrayRep[i]["nomAdministratif"];
                    var prenom = arrayRep[i]["prenomProfesseur"];
                    var mail = arrayRep[i]["mailProfesseurPro"];
                    var telephone = arrayRep[i]["telephoneProfesseur"];
                    var ajout= $('<div class="cadre_resultat_x"><p>' + genre + ' ' + nom + ' ' + prenom + '</p>' +      '<p>' + mail + '</p>' +      '<p>' + telephone + '</p></div>');
                    console.log(ajout.get());
                    $("#resultats_recherche").append(ajout);
                }
           
                
        }); 
    });
    
    $('#onglet_ajout').mousedown(function(){
       window.location.replace("?controller=ajout&action=formprof");
    });
    
    $('#onglet_profil').mousedown(function(){
       window.location.replace("?controller=ajout&action=profil");
    });

	console.log("La mise en place est finie. En attente d'événements...");
});
