<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8"/>
    <title>Recherche de professeur</title>
    <link rel="stylesheet" type="text/css" href="CSS/recherche.css" />
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
    <script src="Views/recherche.js"></script>
</head>
<body>
    <div id="menu">
        <ul id="menu_haut">
            <li class="active onglet" id="onglet_recherche">Recherche</li>
            <li class="onglet" id="onglet_ajout"><a href="?controller=ajout&action=formprof" >Ajout</a></li>
            <li class="onglet" id="onglet_profil"><a href="?controller=ajout&action=">Profil</a></li>
        </ul>
    </div>

    <div id="cadre_general">
        <input type="search" id="barre_recherche" placeholder="Rechercher..."/>
        <div id="recherche_avancee">
            <input class="recherche_nom_prenom" type="search" id="input_nom" placeholder="Nom" />
            <input class="recherche_nom_prenom" type="search" id="input_prenom" placeholder="Prénom" />
            <div id="div_contrat">
                <p id="label_type_contrat">Type de contrat : </p>
                <select id="type_contrat" name="type_contrat" >
                    
                </select>
            </div>

            <div id="div_annee_debut">
                <p id="label_annee_debut">Année de début : </p>
                <input type="search" id="annee_debut" placeholder="AAAA" />
            </div>

            <div id="div_annee_fin">
                <p id="label_annee_fin">Année de fin : </p>
                <input type="search" id="annee_fin" placeholder="AAAA" />
            </div>

            <div id="div_matieres">
                <p id="label_matieres">Matières : </p>
                <input type="search" id="matieres" placeholder="Mathématiques" />
            </div>

            <div id="div_recherche">
                <input type="button" id="bouton_recherche" value="Rechercher" />
            </div>
        </div>
    </div>

    <div id="resultats_recherche">
       
    </div>

    <input type="button" id="bouton_exportation" value="Exporter résultats" />
</body>
</html>
