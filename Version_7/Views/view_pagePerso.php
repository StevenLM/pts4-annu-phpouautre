<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8"/>
    <title>Recherche de professeur</title>
    <link rel="stylesheet" type="text/css" href="CSS/pagePerso.css" />
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
   
    <script src="Views/pagePerso.js"></script>
</head>
<body>
    <div id="menu">
        <ul id="menu_haut">
            <li class="onglet" id="onglet_recherche"><a href="?controller=recherche?action=default">Recherche</a></li>
            <li class="onglet" id="onglet_ajout"><a href="?controller=ajout&action=formprof" >Ajout</a></li>
            <li class="onglet" id="onglet_profil"><a href="?controller=ajout&action=">Profil</a></li>
        </ul>
    </div>

    <div id="cadre_general">
        
        
        <div id="recherche_avancee"  class="back_white">
            <table style="border:1px solid black; border-collapse:collapse; cell-padding:200px;" cellpadding=10>
            <?php
                $cpt=0;
                $tabNoms = array(
                    "Nom Administratif",
                    "Nom de Naissance",
                    "Prénom",
                    "Deuxième prénom",
                    "Troisième prénom",
                    "Mail Professionnel",
                    "Mail Personnel",
                    "Mail de secours",
                    "Téléphone",
                    "Date de naissance",
                    "Genre"
                );
                $iNom = 0;
                foreach($data['req1'][0] as $key => $value){
                    if($cpt>0) {
                        echo '<tr style="border:1px solid black;">';
                        echo '<td style="border: 1px solid black;"><p class=\'info\'>' . '<strong>' . $tabNoms[$iNom] . '</strong></p> </td><td style="width:85%;"><p class=\'info\'>' . $value . '</p></td>';
                        echo '</tr>';
                        $iNom+=1;
                    }
                    $cpt+=1;
                }
            ?>
            </table>
            

           
        </div>
    </div>

    
</body>
</html>
