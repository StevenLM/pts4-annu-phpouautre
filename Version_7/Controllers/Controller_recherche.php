<?php


class Controller_recherche extends Controller{
	
	
	 public function action_default() {
		$this->render("recherche");
      }	
	  
	  
	  public function action_Recherche() {
        $m=Model::get_Model();
		
		$cpt=0;
		$contrat=false;
		
		$req="SELECT * FROM professeurs  ";
		
		if($_GET['annee_fin']!=null){
			if($contrat==false){
				$contrat=true;
				$req.="JOIN  contrats ON id = contrats.idProfesseur";
			}
			
			if($cpt>0){
				$req.=" and ";
			}
			if($cpt==0){
				$req.=" where ";
			}
			$a=$_GET['annee_fin'];
			$req.="YEAR(contrats.anneeFin)='$a'";
			$cpt=1;
		}
		
		if($_GET['annee_debut']!=null){
			if($contrat==false){
				$contrat=true;
				$req.="JOIN  contrats ON id = contrats.idProfesseur";
			}
			
			if($cpt>0){
				$req.=" and ";
			}
			if($cpt==0){
				$req.=" where ";
			}
			$a=$_GET['annee_debut'];
			$req.="YEAR(contrats.anneeDebut)='$a'";
			$cpt=1;
		}
		
		if($_GET['type_contrat']!=null){
			if($contrat==false){
				$contrat=true;
				$req.="JOIN  contrats ON id = contrats.idProfesseur";
			}
			
			if($cpt>0){
				$req.=" and ";
			}
			if($cpt==0){
				$req.=" where ";
			}
			$a=$_GET['type_contrat'];
			$req.="contrats.typeContrat='$a'";
			$cpt=1;
		}
		
		if($_GET['matieres']!=null){
			if($contrat==false){
				$contrat=true;
				$req.="JOIN  contrats ON id = contrats.idProfesseur";
			}
			
			if($cpt>0){
				$req.=" and ";
			}
			if($cpt==0){
				$req.=" where ";
			}
			$a=$_GET['matieres'];
			$req.="contrats.matiere='$a'";
			$cpt=1;
		}
		
		if($_GET['input_nom']!=null){
			if($cpt==0){
				$req.=" where ";
			}
			if($cpt>0){
				$req.=" and ";
			}
			$cpt+=1;
			$a=$_GET['input_nom'];
			$req.=" nomNaissance='$a'";
			
		}
		
		if($_GET['input_prenom']!=null){
			if($cpt==0){
				$req.=" where ";
			}
			if($cpt>0){
				$req.=" and ";
			}
			$a=$_GET['input_prenom'];
			$req.="prenomProfesseur='$a'";
			$cpt+=1;
		}
		
		
		$res=$m->recherche($_GET,$req);
		
		$data=[
		
		"test"=>$res
		
		];
        echo json_encode($data);
		
      }
	  
	  
	  public function action_pagePers(){
		  $m=Model::get_Model();
		  $res1=$m->info_prof($_GET['id']);
		  $res2=$m->info_contrat_prof($_GET['id']);
		  
		  $data=[
		
			"req1"=>$res1,
			"req2"=>$res2
		
		];
		
		$this->render("pagePerso",$data);
	  }
	  
	   public function action_listeContrat(){
		   $m=Model::get_Model();
		   $res=$m->nom_contrat();
		   
		   $data=[
		"test"=>$res
		];  
		  echo json_encode($data);
		   
	   }
	
}



?>