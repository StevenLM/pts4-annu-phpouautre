DROP TABLE IF EXISTS professeurs;
DROP TABLE IF EXISTS contrats;
DROP TABLE IF EXISTS export;

CREATE TABLE professeurs(

	idProfesseur INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
	nomAdministratif VARCHAR(64) NOT NULL,
	nomNaissance VARCHAR(64) NOT NULL,
	prenomProfesseur VARCHAR(64) NOT NULL,
	deuxiemeprenomProfesseur VARCHAR(64) ,
	troisiemeprenomProfesseur VARCHAR(64) ,
	mailProfesseurPro VARCHAR(255) ,
	mailProfesseurPerso VARCHAR(255) ,
	mailProfesseurSecour VARCHAR(255) ,
	telephoneProfesseur VARCHAR(64) ,
	naissance DATE NOT NULL,
	genre VARCHAR(20)
	
	);
	
	
CREATE TABLE contrats(

	idContrat INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
	idProfesseur INT NOT NULL ,
	anneeDebut DATE NOT NULL,
	anneFin DATE NOT NULL,
	typeContrat VARCHAR(255) NOT NULL,
	matiere VARCHAR(255) NOT NULL,
	nbrHeure INT NOT NULL,
	raison TEXT,
	FOREIGN KEY (idProfesseur) references professeurs(idProfesseur)
);

CREATE TABLE export(

idProfesseur INT NOT NULL,
idExport INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
dateExport TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP

);