<?php
class Controller_ajout extends Controller {

	 public function action_default() {
// $this->render("");
  }

  public function action_formprof() {
    $this->render("ajout_prof");
  }

  public function action_addprof() {
    $m = Model::get_Model();
    $prof = $_POST;

    // Vérification que les champs avec une étoile ne sont pas vide
    if( trim($prof['nomNAI']) != "" and !empty($prof['nomNAI']) and
        trim($prof['nomADM']) != "" and !empty($prof['nomADM']) and
        trim($prof['prenom1']) != "" and !empty($prof['prenom1']) ) {

        if(!preg_match("#^[a-z0-9._-]+@[a-z0-9._-]{2,}\.[a-z]{2,4}$#", $prof['mailPro'])) {
          $prof['mailPro'] = "";
        }
        if(!preg_match("#^[a-z0-9._-]+@[a-z0-9._-]{2,}\.[a-z]{2,4}$#", $prof['mailPerso'])) {
          $prof['mailPerso'] = "";
        }
        if(!preg_match("#^[a-z0-9._-]+@[a-z0-9._-]{2,}\.[a-z]{2,4}$#", $prof['mailSecours'])) {
          $prof['mailSecours'] = "";
        }
        if(!preg_match("#^[0-9]{10}$#", $prof['tel'])) {
          $prof['tel'] = "";
        }

        $m->ajout_prof($prof);
        echo '<script>alert("Ajout effectué !")</script>';
        echo '<script>window.location.replace("?controller=ajout&action=formprof");</script>';
    }
    else {
      echo '<script>alert("Les champs avec une étoile de doivent pas être vide")</script>';
      echo '<script>window.location.replace("?controller=ajout&action=formprof");</script>';
    }
    print_r($prof);
  }

}
?>
