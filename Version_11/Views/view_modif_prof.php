<?php 
    if (!(isset($_POST['prenom2']))) {
        $_POST['prenom2']="";
    }
    if (!(isset($_POST['prenom3']))) {
        $_POST['prenom3']="";
    }
    if (!(isset($_POST['prenom2']))) {
        $_POST['prenom2']="";
    }
    if (!(isset($_POST['mailPro']))) {
        $_POST['mailPro']="";
    }
    if (!(isset($_POST['mailPerso']))) {
        $_POST['mailPerso']="";
    }
    if (!(isset($_POST['mailSecours']))) {
        $_POST['mailSecours']="";
    }
    if (!(isset($_POST['tel']))) {
        $_POST['tel']="";
    }

?>

<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8"/>
        <title>Ajout de professeur</title>
        <link rel="stylesheet" type="text/css" href="CSS/ajout.css" />
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
        <script src="modif_prof.js"></script>
    </head>
    <body>
        <div id="menu">
            <ul id="menu_haut">
                <li class="onglet" id="onglet_recherche"><a href="?controller=recherche&action=default" >Recherche</a></li>
                <li class="onglet" id="onglet_ajout"><a href="?controller=ajout&action=formprof" >Ajout</a></li>
                <li class="onglet" id="onglet_profil"><a href="?controller=linkCP&action=linkCP">Profil</a></li>
            </ul>
        </div>

        <form action="?controller=ajout&action=modifProf" method="post" id="cadre_general">
            <div id="bloc_ajout">
                <span id="erreur-nomNAI">/!\ Seulement des caractères pour le nom de naissance !<br/></span>
                <span id="erreur-nomADM">/!\ Seulement des caractères pour le nom administratif !<br/></span>
                <span id="erreur-prenom1">/!\ Seulement des caractères pour le premier prénom !<br/></span>
                <span id="erreur-prenom2">/!\ Seulement des caractères pour le deuxième prénom !<br/></span>
                <span id="erreur-prenom3">/!\ Seulement des caractères pour le troisième prénom !<br/></span>
                <span id="erreur-mailPro">/!\ Mail pro doit être au format : adresse@mail.fr !<br/></span>
                <span id="erreur-mailPerso">/!\ Mail perso doit être au format : adresse@mail.fr !<br/></span>
                <span id="erreur-mailSecours">/!\ Mail de secours doit être au format : adresse@mail.fr !<br/></span>
                <span id="erreur-tel">/!\ Le numéro de téléphone ne doit pas contenir d'espace et avoir 10 nombres !<br/></span>
                <br/>

                <input class="champs" type="text" name="nomNAI" placeholder="Nom de naissance" value="<?=$_POST['nomNAI']?>"/> *
                <input class="champs" type="text" id="droite" name="nomADM" placeholder="Nom administratif"value="<?=$_POST['nomADM']?>" /> *
                <input class="champs" type="text" name="prenom1" placeholder="Premier prénom" value="<?=$_POST['prenom1']?>"/> *
                <input class="champs" type="text" id="droite" name="prenom2" placeholder="Deuxième prénom" value="<?=$_POST['prenom2']?>" />
                <input class="champs" type="text" name="prenom3" placeholder="Troisième prénom" value="<?=$_POST['prenom3']?>" />

                <div id="div_champs">
                    <p id="label_champs" for="sexe">Sexe : </p>
                    <select id="champs" name="sexe">
                        <?php 
                            if (strtolower(trim($_POST['sexe'])) == "homme") : ?>
                                <option value="Homme">Homme</option>
                                <option value="Femme">Femme</option>
                            <?php endif?>
                        
                        <?php 
                            if (strtolower(trim($_POST['sexe'])) == "femme") : ?>
                                <option value="Femme">Femme</option>
                                <option value="Homme">Homme</option>
                            <?php endif?>
                      
                    </select>
                </div>

                <div id="div_champs">
                    <input class="champs" type="text" name="idProf" value="<?=$_POST['idProf']?>"  style="display:none"/>
                  <input class="champs" type="text" name="mailPro" placeholder="Mail Pro" value="<?=$_POST['mailPro']?>"  />
                  <input class="champs" type="text" id="droite" name="mailPerso" placeholder="Mail Perso"value="<?=$_POST['mailPerso']?>"  />
                  <input class="champs" type="text" name="mailSecours" placeholder="Mail de Secours" value="<?=$_POST['mailSecours']?>"  />
                  <input class="champs" type="text" id="droite" name="tel" placeholder="Téléphone" value="<?=$_POST['tel']?>"  />
                </div>

                <div id="div_champs">
                  <p id="label_champs" for="dnais">Date de naissance * : </p>
                  <input id="champs" name="dnais" type="date" value="<?=$_POST['dnais']?>"  />
                </div>

                <br/>

                <div id="div_champs">
                    <input type="submit" id="bouton_ajout" value="Modifier un professeur" />
                </div>
            </div>
        </form>

    </body>
</html>
