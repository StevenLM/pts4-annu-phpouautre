console.log("Ce programme JS vient d'être chargé");

$(document).ready(function()
{
	console.log("Le document est pret");

	$( "#datepicker" ).datepicker();
	$( "#datepicker2" ).datepicker();

	$('input[name=heuresh]').keyup(function() {
      var hs = $('input[name=heuresh]').val();

      if(!/^[0-4]?[0-9]?$/.test(hs)) {
        $('#erreur-heures').show();
        $(this).css('border','1px solid red');
      }
      else {
        $('#erreur-heures').hide();
        $(this).removeAttr('style');
      }
    });

	$('input[name=raison]').keyup(function() {
      var rn = $('input[name=raison]').val();

      if(/[^a-zA-ZáàâäãåçéèêëíìîïñóòôöõúùûüýÿæœÁÀÂÄÃÅÇÉÈÊËÍÌÎÏÑÓÒÔÖÕÚÙÛÜÝŸÆŒ-\s$]/.test(rn)) {
        $('#erreur-raison').show();
        $(this).css('border','1px solid red');
      }
      else {
        $('#erreur-raison').hide();
        $(this).removeAttr('style');
      }
    });


	console.log("La mise en place est finie. En attente d'événements...");
});
