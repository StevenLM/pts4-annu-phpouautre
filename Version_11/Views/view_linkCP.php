<!DOCTYPE html>
<html>
<! ?controller=linkCP&action=linkCP >
<head>
    <meta charset="utf-8"/>
    <title>Ajout de ContractProfesseur</title>
    <link rel="stylesheet" type="text/css" href="CSS/linkCP.css" />
    <!<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"><!</script>
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
    <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
    <script src="Views/linkCP.js"></script>
</head>
<body>
    <div id="menu">
        <ul id="menu_haut">
            <li class="onglet" id="onglet_recherche"><a href="?controller=recherche&action=default" >Recherche</li>
                <li class="active onglet" id="onglet_ajout"><a href="?controller=ajout" >Ajout</a></li>
                <li class="onglet" id="onglet_profil"><a href="?controller=linkCP&action=linkCP">Profil</a></li>
            </ul>
        </div>

        <form action="?controller=linkCP&action=addCP" method="post" id="cadre_general">

            <div id="bloc_ajout" class="back_white">
                <span id="erreur-heures">/!\ Seulement des nombres pour les heures de travail par semaine ! (49 Maximum) <br/></span>
                <span id="erreur-raison">/!\ Seulement des caractères pour la raison !<br/></span>

                <div class="div_champs row">
                    <span class="column">Date début* : <input name="dDebut" type="date" /><!input type="text" id="datepicker" placeholder="Date debut"></span>
                    <span class="column">Date fin* : <input name="dFin" type="date" /><!input type="text" id="datepicker2" placeholder="Date fin"></span>
                </div>

                <div class="div_champs row">
                    <span class="column">
                        Type de contrat* :
                        <select name="typecontract">
                            <?php foreach ($contrats as $value) { ?>
                                <option value="<?php echo $value["nom"] ?>"><?php echo $value["nom"] ?></option>
                            <?php } ?>
                        </select>
                    </span>
                    <span class="column">
                        Matières* :
                        <select name="typematiere">
                            <?php foreach ($matieres as $value2) { ?>
                                <option value="<?php echo $value2["matiere"] ?>"><?php echo $value2["matiere"] ?></option>
                            <?php } ?>
                        </select>
                    </span>
                </div>

                <div class="div_champs row">
                    <span class="column">Heures* (hebdomadaire) : <input type="text" name="heuresh" placeholder="Heures" /></span>
                    <span class="column">Raison* : <input type="text" name="raison" placeholder="Raison" /></span>
                </div>

                <div class="div_champs row">
                    <span class="column">
                        Professeur* :
                        <select name="professeur" >
                            <?php foreach ($nomadmin as $value3) { ?>
                                <option value="<?php echo $value3["id"] ?>"><?php echo $value3["nomNaissance"]; echo " "; echo $value3["prenomProfesseur"]; ?></option>
                            <?php } ?>
                        </select>
                    </span>

                    <span class="column"><input type="submit" id="bouton_relier" value="Relier Contrat" /></span>
                </div>
            </div>

        </form>
    </body>
    </html>
