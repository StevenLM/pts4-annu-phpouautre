<?php

class Model{

	private $bd;

	private static $instance = null;

	/**
	 * Constructeur créant l'objet PDO et l'affectant à $bd
	 */
	private function __construct(){
		$dsn = "mysql:host=localhost;dbname=annuaire";
		$login = "root";
		$password = "";
		$this->bd = new PDO($dsn, $login, $password);
		$this->bd->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
		$this->bd->query("SET nameS 'utf8'");

	}

	public static function get_model() {
        if (is_null(self::$instance))
            self::$instance = new Model();
        return self::$instance;
  }


  public function recherche($GET,$req){

	  // $requete=$this->bd->prepare("select * from professeurs AS p, contrats AS c where p.nomNaissance = :nom and p.prenomProfesseur = :pre and c.typeContrat=:ctr ;");
	  $requete=$this->bd->prepare($req);
	  // $requete->bindValue(':nom',e($GET['input_nom']));
	  // $requete->bindValue(':pre',e($GET['input_prenom']));
	  // $requete->bindValue(':ctr',e($GET['type_contrat']));
	  $requete->execute();
	  $res=$requete->fetchAll(PDO::FETCH_ASSOC);
	  return $res;
  }

	public function ajout_prof($prof) {
		$requete = $this->bd->prepare('INSERT INTO professeurs (id, nomAdministratif, nomNaissance, prenomProfesseur, deuxiemeprenomProfesseur, troisiemeprenomProfesseur, mailProfesseurPro, mailProfesseurPerso, mailProfesseurSecour, telephoneProfesseur, naissance, genre) VALUES (NULL, :nadm, :nnai, :p1, :p2, :p3, :mpro, :mper, :msec, :tel, :dn, :ge)');
		$requete->bindValue(':nadm', e($prof['nomADM']));
		$requete->bindValue(':nnai', e($prof['nomNAI']));
		$requete->bindValue(':p1', e($prof['prenom1']));
		$requete->bindValue(':p2', e($prof['prenom2']));
		$requete->bindValue(':p3', e($prof['prenom3']));
		$requete->bindValue(':mpro', e($prof['mailPro']));
		$requete->bindValue(':mper', e($prof['mailPerso']));
		$requete->bindValue(':msec', e($prof['mailSecours']));
		$requete->bindValue(':tel', e($prof['tel']));
		$requete->bindValue(':dn', e($prof['dnais']));
		$requete->bindValue(':ge', e($prof['sexe']));
		$requete->execute();
	}
    
    public function update_prof($prof){
        $requete = $this->bd->prepare('UPDATE professeurs SET nomAdministratif=:nadm,nomNaissance=:nnai,prenomProfesseur=:p1,deuxiemeprenomProfesseur=:p2,troisiemeprenomProfesseur=:p3,mailProfesseurPro=:mpro,mailProfesseurPerso=:mper,mailProfesseurSecour=:msec,telephoneProfesseur=:tel,naissance=:dn,genre=:ge WHERE id=:id');
        $requete->bindValue(':nadm', e($prof['nomADM']));
		$requete->bindValue(':nnai', e($prof['nomNAI']));
		$requete->bindValue(':p1', e($prof['prenom1']));
		$requete->bindValue(':p2', e($prof['prenom2']));
		$requete->bindValue(':p3', e($prof['prenom3']));
		$requete->bindValue(':mpro', e($prof['mailPro']));
		$requete->bindValue(':mper', e($prof['mailPerso']));
		$requete->bindValue(':msec', e($prof['mailSecours']));
		$requete->bindValue(':tel', e($prof['tel']));
		$requete->bindValue(':dn', e($prof['dnais']));
		$requete->bindValue(':ge', e($prof['sexe']));
        $requete->bindValue(':id', e($prof['idProf']));
		$requete->execute();
    }

    public function ajout_export(){
        $requete = $this->bd->prepare('INSERT INTO export (idProfesseur, idExport, dateExport) VALUES (NULL, NULL, now())');
        $requete->execute();
    }

	public function info_Prof($id){

		$requete=$this->bd->prepare("SELECT * FROM professeurs where id=:id");
		$requete->bindValue(':id',e($id));
		$requete->execute();
		$res=$requete->fetchAll(PDO::FETCH_ASSOC);
		return $res;

	}

	public function info_contrat_prof($id){

		$requete=$this->bd->prepare("SELECT * FROM contrats where idProfesseur=:id");
		$requete->bindValue(':id',e($id));
		$requete->execute();
		$res=$requete->fetchAll(PDO::FETCH_ASSOC);
		return $res;

	}

	public function nom_contrat(){
		$requete=$this->bd->prepare("SELECT nom FROM NOM_CONTRAT ");
		$requete->execute();
		$res=$requete->fetchAll(PDO::FETCH_ASSOC);
		return $res;
	}

	public function nom_matiere(){
		$requete=$this->bd->prepare("SELECT matiere FROM nom_matiere ");
		$requete->execute();
		$res=$requete->fetchAll(PDO::FETCH_ASSOC);
		return $res;
	}

	public function nom_prof(){
		$requete=$this->bd->prepare("SELECT id, nomNaissance, prenomProfesseur FROM professeurs ");
		$requete->execute();
		$res=$requete->fetchAll(PDO::FETCH_ASSOC);
		return $res;
	}

	public function linkCP($data){
        $requete = $this->bd->prepare('INSERT INTO contrats (idProfesseur, anneeDebut, anneeFin, typeContrat, matiere, nbrHeure, raison) VALUES (:id, :dd, :df, :tc, :tm, :he, :rn)');
		$requete->bindValue(':id', e($data['idProf']));
		$requete->bindValue(':dd', e($data['aDebut']));
		$requete->bindValue(':df', e($data['aFin']));
		$requete->bindValue(':tc', e($data['tContrat']));
		$requete->bindValue(':tm', e($data['matiere']));
		$requete->bindValue(':he', e($data['heures']));
		$requete->bindValue(':rn', e($data['raison']));
		$requete->execute();
    }
}
