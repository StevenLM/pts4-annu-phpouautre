CREATE DATABASE IF NOT EXISTS annuaire;
USE annuaire;
DROP TABLE IF EXISTS professeurs;
DROP TABLE IF EXISTS contrats;
DROP TABLE IF EXISTS export;
DROP TABLE IF EXISTS NOM_CONTRAT;
DROP TABLE IF EXISTS nom_matiere;

CREATE TABLE professeurs(

	id INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
	nomAdministratif VARCHAR(64) NOT NULL,
	nomNaissance VARCHAR(64) NOT NULL,
	prenomProfesseur VARCHAR(64) NOT NULL,
	deuxiemeprenomProfesseur VARCHAR(64) ,
	troisiemeprenomProfesseur VARCHAR(64) ,
	mailProfesseurPro VARCHAR(255) ,
	mailProfesseurPerso VARCHAR(255) ,
	mailProfesseurSecour VARCHAR(255) ,
	telephoneProfesseur VARCHAR(64) ,
	naissance DATE NOT NULL,
	genre VARCHAR(20)

	);


CREATE TABLE contrats(

	idContrat INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
	idProfesseur INT NOT NULL ,
	anneeDebut DATE NOT NULL,
	anneeFin DATE NOT NULL,
	typeContrat VARCHAR(255) NOT NULL,
	matiere VARCHAR(255) NOT NULL,
	nbrHeure INT NOT NULL,
	raison TEXT
    -- FOREIGN KEY (idProfesseur) references professeurs(idProfesseur)
);

CREATE TABLE export(

idProfesseur INT,
idExport INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
dateExport TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP

);

CREATE TABLE NOM_CONTRAT(

	nom TEXT NOT NULL

);

CREATE TABLE nom_matiere(

	matiere TEXT NOT NULL

);

INSERT INTO `professeurs` (`id`, `nomAdministratif`, `nomNaissance`, `prenomProfesseur`, `deuxiemeprenomProfesseur`, `troisiemeprenomProfesseur`, `mailProfesseurPro`, `mailProfesseurPerso`, `mailProfesseurSecour`, `telephoneProfesseur`, `naissance`, `genre`) VALUES (NULL, 'JOHN', 'JOHN', 'Johnson', NULL, NULL, 'john.johnson@exemple.org', NULL, NULL, '0000000000', '1982-02-13', 'Homme');
INSERT INTO nom_matiere values ("Anglais");
INSERT INTO nom_matiere values ("Web riche");
INSERT INTO nom_matiere values ("Projet");

INSERT INTO NOM_CONTRAT values ("AFTER");
INSERT INTO NOM_CONTRAT values ("MCF");
INSERT INTO NOM_CONTRAT values ("PU");
INSERT INTO NOM_CONTRAT values ("ENS");
INSERT INTO NOM_CONTRAT values ("PAST");
INSERT INTO NOM_CONTRAT values ("1/2 PAST");
INSERT INTO NOM_CONTRAT values ("Moniteur");
INSERT INTO NOM_CONTRAT values ("Vacataire");
INSERT INTO NOM_CONTRAT values ("1/2 Vacataire");
INSERT INTO NOM_CONTRAT values ("CDD");
INSERT INTO NOM_CONTRAT values ("CDI");
INSERT INTO NOM_CONTRAT values ("Temps partiel");
