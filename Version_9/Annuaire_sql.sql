DROP TABLE IF EXISTS professeurs;
DROP TABLE IF EXISTS contrats;
DROP TABLE IF EXISTS export;
DROP TABLE IF EXISTS NOM_CONTRAT;
 
CREATE TABLE professeurs(

	id INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
	nomAdministratif VARCHAR(64) NOT NULL,
	nomNaissance VARCHAR(64) NOT NULL,
	prenomProfesseur VARCHAR(64) NOT NULL,
	deuxiemeprenomProfesseur VARCHAR(64) ,
	troisiemeprenomProfesseur VARCHAR(64) ,
	mailProfesseurPro VARCHAR(255) ,
	mailProfesseurPerso VARCHAR(255) ,
	mailProfesseurSecour VARCHAR(255) ,
	telephoneProfesseur VARCHAR(64) ,
	naissance DATE NOT NULL,
	genre VARCHAR(20)

	);


CREATE TABLE contrats(

	idContrat INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
	idProfesseur INT NOT NULL ,
	anneeDebut DATE NOT NULL,
	anneeFin DATE NOT NULL,
	typeContrat VARCHAR(255) NOT NULL,
	matiere VARCHAR(255) NOT NULL,
	nbrHeure INT NOT NULL,
	raison TEXT,
	FOREIGN KEY (idProfesseur) references professeurs(idProfesseur)
);

CREATE TABLE export(

idProfesseur INT,
idExport INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
dateExport TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP

);

CREATE TABLE NOM_CONTRAT(

	nom TEXT NOT NULL 

);


INSERT INTO NOM_CONTRAT values ("AFTER");
INSERT INTO NOM_CONTRAT values ("MCF");
INSERT INTO NOM_CONTRAT values ("PU");
INSERT INTO NOM_CONTRAT values ("ENS");
INSERT INTO NOM_CONTRAT values ("PAST");
INSERT INTO NOM_CONTRAT values ("1/2 PAST");
INSERT INTO NOM_CONTRAT values ("Moniteur");
INSERT INTO NOM_CONTRAT values ("Vacataire");
INSERT INTO NOM_CONTRAT values ("1/2 Vacataire");
INSERT INTO NOM_CONTRAT values ("CDD");
INSERT INTO NOM_CONTRAT values ("CDI");
INSERT INTO NOM_CONTRAT values ("Temps partiel");
