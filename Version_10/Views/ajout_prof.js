console.log("Ce programme JS vient d'être chargé");
$(document).ready(function()
{
	console.log("Le document est pret");

  // Vérification que l'input contient uniquement des caractères
  $('input[name=nomNAI]').keyup(function() {
    var nomNAI = $('input[name=nomNAI]').val();
    nomNAI = nomNAI.toUpperCase();
    $('input[name=nomNAI]').val(nomNAI);

    if(/[^a-zA-ZáàâäãåçéèêëíìîïñóòôöõúùûüýÿæœÁÀÂÄÃÅÇÉÈÊËÍÌÎÏÑÓÒÔÖÕÚÙÛÜÝŸÆŒ-\s]/.test(nomNAI)) {
      $('#erreur-nomNAI').show();
      $(this).css('border','1px solid red');
    } else {
      $('#erreur-nomNAI').hide();
      $(this).removeAttr('style');
    }
  });

  $('input[name=nomADM]').keyup(function() {
    var nomADM = $('input[name=nomADM]').val();
    nomADM = nomADM.toUpperCase();
    $('input[name=nomADM]').val(nomADM);

    if(/[^a-zA-ZáàâäãåçéèêëíìîïñóòôöõúùûüýÿæœÁÀÂÄÃÅÇÉÈÊËÍÌÎÏÑÓÒÔÖÕÚÙÛÜÝŸÆŒ-\s]/.test(nomADM)) {
      $('#erreur-nomADM').show();
      $(this).css('border','1px solid red');
    } else {
      $('#erreur-nomADM').hide();
      $(this).removeAttr('style');
    }
  });

	$('input[name=prenom1]').keyup(function() {
    var prenom = $('input[name=prenom1]').val();
    prenom = majApresTiret(prenom);
    $(this).val(prenom);

    if(/[^a-zA-ZáàâäãåçéèêëíìîïñóòôöõúùûüýÿæœÁÀÂÄÃÅÇÉÈÊËÍÌÎÏÑÓÒÔÖÕÚÙÛÜÝŸÆŒ-]/.test(prenom)) {
      $('#erreur-prenom1').show();
      $(this).css('border','1px solid red');
    }
    else {
      $('#erreur-prenom1').hide();
      $(this).removeAttr('style');
    }
  });

	$('input[name=prenom2]').keyup(function() {
    var prenom = $('input[name=prenom2]').val();
    prenom = majApresTiret(prenom);
    $(this).val(prenom);

    if(/[^a-zA-ZáàâäãåçéèêëíìîïñóòôöõúùûüýÿæœÁÀÂÄÃÅÇÉÈÊËÍÌÎÏÑÓÒÔÖÕÚÙÛÜÝŸÆŒ-]/.test(prenom)) {
      $('#erreur-prenom2').show();
      $(this).css('border','1px solid red');
    }
    else {
      $('#erreur-prenom2').hide();
      $(this).removeAttr('style');
    }
  });

	$('input[name=prenom3]').keyup(function() {
    var prenom = $('input[name=prenom3]').val();
    prenom = majApresTiret(prenom);
    $(this).val(prenom);

    if(/[^a-zA-ZáàâäãåçéèêëíìîïñóòôöõúùûüýÿæœÁÀÂÄÃÅÇÉÈÊËÍÌÎÏÑÓÒÔÖÕÚÙÛÜÝŸÆŒ-]/.test(prenom)) {
      $('#erreur-prenom3').show();
      $(this).css('border','1px solid red');
    }
    else {
      $('#erreur-prenom3').hide();
      $(this).removeAttr('style');
    }
  });

  // Vérification que l'input est une adresse mail
  $('input[name=mailPro]').keyup(function() {
    var mail = $('input[name=mailPro]').val();

    if(!isEmail(mail) && mail.length != 0) {
      $('#erreur-mailPro').show();
      $(this).css('border','1px solid red');
    }
    else {
      $('#erreur-mailPro').hide();
      $(this).removeAttr('style');
    }
  });

	$('input[name=mailPerso]').keyup(function() {
    var mail = $('input[name=mailPerso]').val();

    if(!isEmail(mail) && mail.length != 0) {
      $('#erreur-mailPerso').show();
      $(this).css('border','1px solid red');
    }
    else {
      $('#erreur-mailPerso').hide();
      $(this).removeAttr('style');
    }
  });

	$('input[name=mailSecours]').keyup(function() {
    var mail = $('input[name=mailSecours]').val();

    if(!isEmail(mail) && mail.length != 0) {
      $('#erreur-mailSecours').show();
      $(this).css('border','1px solid red');
    }
    else {
      $('#erreur-mailSecours').hide();
      $(this).removeAttr('style');
    }
  });

  // Vérification que l'input contient uniquement 10 nombres
  $('input[name=tel]').keyup(function() {
    var tel = $('input[name=tel]').val();

    if(!isNumTel(tel) && tel.length != 0) {
      $('#erreur-tel').show();
      $(this).css('border','1px solid red');
    }
    else {
      $('#erreur-tel').hide();
      $(this).removeAttr('style');
    }
  });

  // AJOUT si toutes les vérifications sont OK
  $('#bouton_ajout').click(function() {

    // CHAMPS OBLIGATOIRES
      // NOMS
      if( $('input[name=nomNAI]').val().length != 0) {
        if( $("#erreur-nomNAI").is(':visible') ) {
          alert("<NOM DE NAISSANCE> doit être correctement remplis ! "); return false;
        }
      } else { alert("<NOM DE NAISSANCE> ne doit pas être vide ! "); return false; }

      if( $('input[name=nomADM]').val().length != 0) {
        if( $("#erreur-nomADM").is(':visible') ) {
          alert("<NOM ADMINISTRATIF> doit être correctement remplis ! ");
					return false;
        }
      } else { alert("<NOM ADMINISTRATIF> ne doit pas être vide ! "); return false; }

      // PRENOM
      if( $('input[name=prenom1]').val().length != 0) {
        var prenom = $('input[name=prenom1]').val();
        if( $("#erreur-prenom1").is(':visible')) {
            alert("<PREMIER PRÉNOM> doit être correctement remplis ! ");
						return false;
        }
      } else { alert("<PREMIER PRÉNOM> ne doit pas être vide ! "); return false; }

      // DATE DE NAISSANCE
      if( $('input[name=dnais]').val().length === 0) {
          alert("<DATE DE NAISSANCE> doit être correctement remplis ! ");
					 return false;
      }

    // CHAMPS NON OBLIGATOIRES
      // PRENOMS
      if( $("#erreur-prenom2").is(':visible')) {
          alert("<DEUXIÈME PRÉNOM> doit être correctement remplis ! ");
					return false;
      }
			if( $("#erreur-prenom3").is(':visible')) {
          alert("<TROISIÈME PRÉNOM> doit être correctement remplis ! ");
					return false;
      }

      // MAILS
			if( $("#erreur-mailPro").is(':visible')) {
          alert("<MAIL PRO> doit être correctement remplis ! ");
					return false;
      }
			if( $("#erreur-mailPerso").is(':visible')) {
          alert("<MAIL PERSO> doit être correctement remplis ! ");
					return false;
      }
			if( $("#erreur-mailSecours").is(':visible')) {
          alert("<MAIL DE SECOURS> doit être correctement remplis ! ");
					return false;
      }

      // TELEPHONE
			if( $("#erreur-tel").is(':visible')) {
          alert("<TÉLÉPHONE> doit être correctement remplis ! ");
					return false;
      }

  });

	// IMPORT CSV
	$('#bouton_csv').click(function() {
		// S'il n'y a pas de fichier, bouton désactivé
		if( $('#file').val() === '' ) {
			return false;
		}
	});

	// NAVIGATION
	$('#onglet_recherche').mousedown(function(){
		 window.location.replace("?controller=recherche");
	});

	$('#onglet_ajout').mousedown(function(){
		 window.location.replace("?controller=ajout");
	});

	$('#ajout-contrat').mousedown(function(){
		 window.location.replace("?controller=linkCP&action=linkCP");
	});

	$('#ajout-prof').mousedown(function(){
		 window.location.replace("?controller=ajout&action=formprof");
	});

	console.log("La mise en place est finie. En attente d'événements...");
});

function majApresTiret(str) {
  var lower = String(str).toLowerCase();
  return lower.replace(/(^|-)(\w)/g, function(x) {
    return x.toUpperCase();
  });
}

function isNumTel(tel) {
  var regex = /^[0-9]{10}$/;
  return regex.test(tel);
}

function isEmail(email) {
  var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
  return regex.test(email);
}
