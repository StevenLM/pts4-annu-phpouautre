<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8"/>
        <title>Choix de l'ajout</title>
        <link rel="stylesheet" type="text/css" href="CSS/ajout.css" />
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
        <script src="Views/ajout_prof.js"></script>
    </head>
    <body>
      <div id="menu">
          <ul id="menu_haut">
              <li class="onglet" id="onglet_recherche"><a href="?controller=recherche&action=defau	lt" >Recherche</li>
              <li class="active onglet" id="onglet_ajout"><a href="?controller=ajout&action=formprof" >Ajout</a></li>
              <li class="onglet" id="onglet_profil"><a href="?controller=linkCP&action=linkCP">Profil</a></li>
          </ul>
      </div>

      <div id="cadre_general" style="text-align: center">
        <input type="button" id="ajout-prof" value="Ajout d'un professeur" />
        <input type="button" id="ajout-contrat" value="Ajout d'un contrat" />
      </div>

    </body>
</html>
