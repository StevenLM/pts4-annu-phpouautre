<?php 

class Model{

	private $bd;

	private static $instance = null;

	/**
	 * Constructeur créant l'objet PDO et l'affectant à $bd
	 */
	private function __construct(){
		$dsn = "mysql:host=localhost;dbname=annuaire";      
		$login = "root";    
		$password = ""; 
		$this->bd = new PDO($dsn, $login, $password);
		$this->bd->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
		$this->bd->query("SET nameS 'utf8'");
	
	}

	public static function get_model() {
        if (is_null(self::$instance))
            self::$instance = new Model();
        return self::$instance;
  }
  
  
  public function recherche($GET,$req){
	  
	  // $requete=$this->bd->prepare("select * from professeurs AS p, contrats AS c where p.nomNaissance = :nom and p.prenomProfesseur = :pre and c.typeContrat=:ctr ;");
	  $requete=$this->bd->prepare($req);	
	  // $requete->bindValue(':nom',e($GET['input_nom']));
	  // $requete->bindValue(':pre',e($GET['input_prenom']));
	  // $requete->bindValue(':ctr',e($GET['type_contrat']));
	  $requete->execute();
	  $res=$requete->fetchAll(PDO::FETCH_ASSOC);
	  return $res;
  }

}
