console.log("Ce programme JS vient d'être chargé");
$(document).ready(function()
{
	console.log("Le document est pret");
    var stockData=[];
    $.get('?controller=recherche&action=listeContrat',
             {
            
            },
              function(reponse){
                    
                    var arrayRep = jQuery.parseJSON(reponse)["test"];
                    console.log(arrayRep);
                    for (var i = 0; i<arrayRep.length;i+=1){
                        $("#type_contrat").append('<option value=\'' + arrayRep[i]['nom'] + '\'>' + arrayRep[i]['nom'] + '</option>');
                    }
             
             
    });
    
    $('#bouton_recherche').mousedown(function(){        
       $.get('?controller=recherche&action=Recherche',
            {
                input_nom: $('#input_nom').val(),
                input_prenom: $("#input_prenom").val(),
				type_contrat: $("#type_contrat").val(),
				annee_fin: $("#annee_fin").val(),
				annee_debut: $("#annee_debut").val(),
				matieres: $("#matieres").val()
            },
             function(reponse){
                $("#resultats_recherche").html("");
                console.log(reponse);
                var arrayRep = jQuery.parseJSON(reponse)["test"];
                stockData = arrayRep;
                console.log(arrayRep);
                var arrayProfAffiches=[];
                for (var i = 0; i<arrayRep.length;i+=1){
                    if(!(arrayProfAffiches.includes(arrayRep[i]["id"]))) {
                        var genre = arrayRep[i]["genre"];
                        console.log(genre);
                        if (genre.toLowerCase()=='homme') {
                            genre="Mr."
                        }
                        else if (genre.toLowerCase()=='femme'){
                            genre='Mme.';
                        }
                        var nom = arrayRep[i]["nomAdministratif"];
                        var prenom = arrayRep[i]["prenomProfesseur"];
                        var mail = arrayRep[i]["mailProfesseurPro"];
                        var telephone = arrayRep[i]["telephoneProfesseur"];
                        var ajout= $('<div class="cadre_resultat_x"><p>' + genre + ' ' + nom + ' ' + prenom + '</p>' +      '<p>' + mail + '</p>' +      '<p>' + telephone + '</p></div>');
                        console.log(ajout.get());
                        $("#resultats_recherche").append(ajout);
                        arrayProfAffiches[i] = arrayRep[i]['id'];
                    }
                    
                }
           
                
        }); 
    });
    
    $('#bouton_exportation').mousedown(function(){
        downloadCSV({filename:"resultats_recherche.csv"});
    });
    
    $('#onglet_ajout').mousedown(function(){
       window.location.replace("?controller=ajout&action=formprof");
    });
    
    $('#onglet_profil').mousedown(function(){
       window.location.replace("?controller=ajout&action=profil");
    });
    
    
     function convertArrayOfObjectsToCSV(args) {
        var result, ctr, keys, columnDelimiter, lineDelimiter, data;

        data = args.data || null;
        if (data == null || !data.length) {
            return null;
        }

        columnDelimiter = args.columnDelimiter || ',';
        lineDelimiter = args.lineDelimiter || '\n';

        keys = Object.keys(data[0]);

        result = '';
        result += keys.join(columnDelimiter);
        result += lineDelimiter;

        data.forEach(function(item) {
            ctr = 0;
            keys.forEach(function(key) {
                if (ctr > 0) result += columnDelimiter;

                result += item[key];
                ctr++;
            });
            result += lineDelimiter;
        });

        return result;
    }
    
    
     function downloadCSV(args) {
        var data, filename, link;

        var csv = convertArrayOfObjectsToCSV({
            data: stockData
        });
        if (csv == null) return;

        filename = args.filename || 'export.csv';

        if (!csv.match(/^data:text\/csv/i)) {
            csv = 'data:text/csv;charset=utf-8,' + csv;
        }
        data = encodeURI(csv);

        link = document.createElement('a');
        link.setAttribute('href', data);
        link.setAttribute('download', filename);
        link.click();
    }

	console.log("La mise en place est finie. En attente d'événements...");
});
