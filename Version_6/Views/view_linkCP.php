<!DOCTYPE html>
<html>
<! ?controller=linkCP&action=linkCP >
<head>
    <meta charset="utf-8"/>
    <title>Ajout de ContractProfesseur</title>
    <link rel="stylesheet" type="text/css" href="CSS/linkCP.css" />
    <!<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"><!</script>
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
    <link rel="stylesheet" href="/resources/demos/style.css">
    <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
    <script src="Views/linkCP.js"></script>
</head>
<body>
    <div id="menu">
        <ul id="menu_haut">
            <li class="onglet" id="onglet_recherche"><a href="?controller=recherche&action=default" >Recherche</li>
            <li class="active onglet" id="onglet_ajout"><a href="?controller=ajout&action=formprof" >Ajout</a></li>
            <li class="onglet" id="onglet_profil"><a href="?controller=linkCP&action=linkCP">Profil</a></li>
        </ul>
    </div>

    <form action="?controller=linkCP&action=addCP" method="post" id="cadre_general">

        <div id="bloc_ajout">
            <div class="div_champs row">
                <span class="column">Date début : <input type="text" id="datepicker" placeholder="Date debut"></span>
                <span class="column">Date fin : <input type="text" id="datepicker2" placeholder="Date fin"></span>
            </div>

            <div class="div_champs row">
                <span class="column">
                    Type de contrat : <select name="typecontract">
                        <option value="cdi">CDI</option>
                        <option value="cdd">CDD</option>
                    </select>
                </span>
                <span class="column">
                    Matières : <select name="typematiere">
                        <option value="EX">Anglais</option>
                        <option value="XE">Projet</option>
                    </select>
                </span>
            </div>

            <div class="div_champs row">
                <span class="column">Heures (hebdomadaire) : <input type="text" name="heuresh" placeholder="Heures" /></span>
                <span class="column">Raison : <input type="text" name="raison" placeholder="Raison" /></span>
            </div>

            <div class="div_champs center">
                Professeur : <select name="professeur">
                    <option value="EX">Prof1</option>
                    <option value="XE">Prof2</option>
                </div>

                <div class="div_champs center">
                    <input type="submit" id="bouton_relier" value="Relier Contrat" />
                </div>
            </div>

        </form>
    </body>
    </html>
