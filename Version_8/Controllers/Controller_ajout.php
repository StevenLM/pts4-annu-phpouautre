<?php
class Controller_ajout extends Controller {

	 public function action_default() {
		 $this->render("choix_ajout");
  }

  public function action_formprof() {
    $this->render("ajout_prof");
  }

  public function action_addprof() {
    $m = Model::get_Model();
    $prof = $_POST;
    $m->ajout_prof($prof);
		echo '<script>alert("Ajout effectué");</script>';
		echo '<script>window.location.replace("?controller=ajout&action=formprof");</script>';
  }

	public function action_import() {
		$m = Model::get_Model();

			if (isset($_POST["import"])) {
		    $fileName = $_FILES["file"]["tmp_name"];
		    if ($_FILES["file"]["size"] > 0) {
		        $file = fopen($fileName, "r");

						$column = fgetcsv($file, 10000);
						$nbChamps = substr_count($column[0], ',');
						$ok = FALSE;

		        while ($column !== FALSE && $nbChamps == 10) {
								$column = explode(',', $column[0]);
								$column = str_replace('"', ' ', $column);

								$prof = [
									'nomADM' => $column[0],
									'nomNAI' => $column[1],
									'prenom1' => $column[2],
									'prenom2' => $column[3],
									'prenom3' => $column[4],
									'mailPro' => $column[5],
									'mailPerso' => $column[6],
									'mailSecours' => $column[7],
									'tel' => $column[8],
									'dnais' => $column[9],
									'sexe' => $column[10]
								];

								$m->ajout_prof($prof);
								$column = fgetcsv($file, 10000);
								$nbChamps = substr_count($column[0], ',');
								$ok = TRUE;
		        }
						// END WHILE
						if($ok === TRUE) {
							echo '<script>alert("Importation effectué")</script>';
						} else {
							echo '<script>alert("Erreur importation")</script>';
						}
						echo '<script>window.location.replace("?controller=ajout&action=formprof");</script>';
		    }
		}
		else {
			echo '<script>window.location.replace("?controller=ajout&action=formprof");</script>';
		}

	}

}
?>
