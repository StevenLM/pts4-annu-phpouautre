<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8"/>
        <title>Ajout de professeur</title>
        <link rel="stylesheet" type="text/css" href="CSS/ajout.css" />
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
        <script src="Views/ajout_prof.js"></script>
    </head>
    <body>
        <div id="menu">
            <ul id="menu_haut">
                <li class="onglet" id="onglet_recherche"><a href="?controller=recherche&action=defau	lt" >Recherche</li>
                <li class="active onglet" id="onglet_ajout"><a href="?controller=ajout&action=formprof" >Ajout</a></li>
                <li class="onglet" id="onglet_profil"><a href="?controller=linkCP&action=linkCP">Profil</a></li>
            </ul>
        </div>

        <form action="?controller=ajout&action=addprof" method="post" id="cadre_general">
            <div id="bloc_ajout">
                <span id="erreur-nomNAI">/!\ Seulement des caractères pour le nom de naissance !<br/></span>
                <span id="erreur-nomADM">/!\ Seulement des caractères pour le nom administratif !<br/></span>
                <span id="erreur-prenom1">/!\ Seulement des caractères pour le premier prénom !<br/></span>
                <span id="erreur-prenom2">/!\ Seulement des caractères pour le deuxième prénom !<br/></span>
                <span id="erreur-prenom3">/!\ Seulement des caractères pour le troisième prénom !<br/></span>
                <span id="erreur-mailPro">/!\ Mail pro doit être au format : adresse@mail.fr !<br/></span>
                <span id="erreur-mailPerso">/!\ Mail perso doit être au format : adresse@mail.fr !<br/></span>
                <span id="erreur-mailSecours">/!\ Mail de secours doit être au format : adresse@mail.fr !<br/></span>
                <span id="erreur-tel">/!\ Le numéro de téléphone ne doit pas contenir d'espace et avoir 10 nombres !<br/></span>
                <br/>

                <input class="champs" type="text" name="nomNAI" placeholder="Nom de naissance" /> *
                <input class="champs" type="text" id="droite" name="nomADM" placeholder="Nom administratif" /> *
                <input class="champs" type="text" name="prenom1" placeholder="Premier prénom" /> *
                <input class="champs" type="text" id="droite" name="prenom2" placeholder="Deuxième prénom" />
                <input class="champs" type="text" name="prenom3" placeholder="Troisième prénom" />

                <div id="div_champs">
                    <p id="label_champs" for="sexe">Sexe : </p>
                    <select id="champs" name="sexe">
                      <option value="Homme">Homme</option>
                      <option value="Femme">Femme</option>
                    </select>
                </div>

                <div id="div_champs">
                  <input class="champs" type="text" name="mailPro" placeholder="Mail Pro" />
                  <input class="champs" type="text" id="droite" name="mailPerso" placeholder="Mail Perso" />
                  <input class="champs" type="text" name="mailSecours" placeholder="Mail de Secours" />
                  <input class="champs" type="text" id="droite" name="tel" placeholder="Téléphone" />
                </div>

                <div id="div_champs">
                  <p id="label_champs" for="dnais">Date de naissance * : </p>
                  <input id="champs" name="dnais" type="date" />
                </div>

                <br/>

                <div id="div_champs">
                    <input type="submit" id="bouton_ajout" value="Ajouter un professeur" />
                </div>
            </div>
        </form>

        <form action="?controller=ajout&action=import" method="post" enctype="multipart/form-data" id="cadre_general">
          <div id="bloc_ajout">
            <table style="margin: auto" width="50%">
              <tr>
                <td width="60%">
                  <label for="file"><strong>Importer :</strong></label>
                  <input type="file" name="file" id="file" accept=".csv">
                </td>
                <td>
                  <input type="submit" id="bouton_csv" name="import" value="Importer (csv)" />
                </td>
              </tr>
            </table>
          </div>
        </form>

    </body>
</html>
